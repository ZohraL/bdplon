<?php

namespace App\Controller;

use App\Repository\BDRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BDController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(BDRepository $repo)
    {
        return $this->render('bd/index.html.twig', [
            'bd' => $repo->findAll()
        ]);
    }


    
}
